<?php  

	require('inc/fonction.php');

	$categorie   = $_POST['categorie'];
	$nom         = $_POST['nom'];
	$description = $_POST['description'];
	$marque      = $_POST['marque'];
	$pointure    = $_POST['pointure'];
	$prix        = $_POST['prix'];
	$dateajout   = $_POST['dateajout'];

	if($prix <= 0)
	{
		header('location:erreur.php');
	}
	else
	{
		ajouterarticle($categorie,$nom,$description,$marque,$pointure,$prix,$dateajout);
		header('location:article.php');
	}

?>