<?php  

    require('inc/fonction.php');

    $modepaie = listemodepaiementall();

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Mode de paiement</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a class="simple-text">
                    Menu
                </a>
            </div>

            <?php include('inc/menunav.php'); ?>

    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Mode de paiement</a>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Insertion</h3>
                                <h4 class="title">Ajout d'une nouvelle mode paiement</h4>
                            </div>
                            <div class="content">
                                <form action="ajoutarticle.php" method="post">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Nom</label>
                                            <input type="text" class="form-control" name="nom">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Insertion</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des modes paiement</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Mode paiement</th>                                          
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($modepaie as $mo) { ?>
                                        <tr>
                                            <td><?php echo $mo['id']; ?></td>
                                            <td><?php echo $mo['modepaiement']; ?></td>
                                            <td><a href="suppmodepaie.php?nom=<?php echo $mo['modepaiement']; ?>"><button type="submit" class="btn btn-danger">Supprimer</button></a> <a href="modifier.php"><button type="submit" class="btn btn-warning">Modifier</button></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">

            </div>
        </footer>

    </div>
</div>


</body>

    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
    <script src="assets/js/chartist.min.js"></script>
    <script src="assets/js/bootstrap-notify.js"></script>
    <script src="assets/js/light-bootstrap-dashboard.js"></script>
    <script src="assets/js/demo.js"></script>

    <script src="assets/lib/jquery/jquery.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.dataTables.js"></script>
    <script src="assets/js/datatables.js"></script>
    <script src="assets/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/jquery.tablesorter.min.js"></script>
    <script src="assets/js/jquery.tablesorter.js"></script>

    <script>
        $(document).ready(function() {
            $('table.display').DataTable();
        } );
    </script>

</html>