<ul class="nav">
    <li>
        <a href="dashboard.php">
            <i class="pe-7s-graph"></i>
            <p>Tableau de bord</p>
        </a>
    </li>
    <li>
        <a href="article.php">
            <i class="pe-7s-diamond"></i>
            <p>Article</p>
        </a>
    </li>
    <li>
        <a href="categorie.php">
            <i class="pe-7s-portfolio"></i>
            <p>Categorie</p>
        </a>
    </li>
    <li>
        <a href="marque.php">
            <i class="pe-7s-news-paper"></i>
            <p>Marque</p>
        </a>
    </li>
    <li>
        <a href="panier.php">
            <i class="pe-7s-shopbag"></i>
            <p>Panier</p>
        </a>
    </li>
    <li>
        <a href="modepaiement.php">
            <i class="pe-7s-cash"></i>
            <p>Mode de paiement</p>
        </a>
    </li>
    <li>
        <a href="client.php">
            <i class="pe-7s-user"></i>
            <p>Client</p>
        </a>
    </li>
    <li>
        <a href="deconnexion.php">
            <i class="pe-7s-close-circle"></i>
            <p>Deconnexion</p>
        </a>
    </li>
</ul>