<?php

	//------------------------------------------------------------------CONNECTION

    function dbconnect()
	{
		$user='id5305420_root15';
        $pass='root15';
		$dsn='mysql:host=localhost;dbname=id5305420_andiashoes';
		$dbh = new PDO($dsn, $user, $pass);
		 try {
        } catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $dbh;
	}

	//-----------------------------------------------------------------------LISTE


		//ADMINISTRATION

	function loginadmin($nom,$mdp)
	{
		$requete = "SELECT identifiant,motdepasse FROM administrateur where identifiant = '".$nom."' and motdepasse = sha1('".$mdp."')";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

			//ARTICLE

	function listearticle()
	{
		$requete = "SELECT * FROM article";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listearticle_article($id)
	{
		$requete = "SELECT * FROM article Where nom = '".$id."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listearticle_nomcat($categorie)
	{
		$requete = "SELECT categorie FROM article where nom = '".$categorie."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listearticle_categorie($categorie)
	{
		$requete = "SELECT * FROM article where categorie = '".$categorie."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listearticle_recherche($nom)
	{
		$requete = "SELECT * FROM article where nom = '".$nom."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listearticle_recherche_multicritere($date1,$date2,$nom,$categorie,$marque,$pu1,$pu2)
	{
		$requete = "SELECT * FROM article where dateajout > '".$date1."' and dateajout < '".$date2."' and nom like '%".$nom."%' and categorie like '%".$categorie."%' and marque like '%".$marque."%' and prix > ".$pu1." and prix < ".$pu2." ";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
		}
		echo $requete;
		return $donne;
	}

		//CATEGORIE

	function listecategorie()
	{
		$requete = "SELECT * FROM categorie";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//MARQUE

	function listemarque()
	{
		$requete = "SELECT * FROM marque";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//CLIENT

	function listeclient()
	{
		$requete = "SELECT * FROM client";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listeclient_log($identifiant,$motdepasse)
	{
		$requete = "SELECT identifiant,motdepasse FROM client WHERE identifiant = '".$identifiant."' AND motdepasse = sha1('".$motdepasse."')";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listeclient_recherche($identifiant)
	{
		$requete = "SELECT * FROM client where identifiant = '".$identifiant."' ";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listeclient_solde($identifiant)
	{
		$requete = "SELECT solde FROM client where identifiant = '".$identifiant."' ";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//PANIER

	function listepanier($nom)
	{
		$requete = "SELECT * FROM panier Where client = '".$nom."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listepanier_somme($nom)
	{
		$requete = "SELECT SUM(montant) as montant FROM panier Where client = '".$nom."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//HISTORIQUE

	function listehistorique()
	{
		$requete = "SELECT * FROM historiquepanier";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//IMAGE

	function listeimage($nom)
	{
		$requete = "SELECT image FROM image where article = '".$nom."'";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//MODEPAIEMENT

	function listemodepaiementall()
	{
		$requete = "SELECT * FROM modepaiement";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	function listemodepaiement()
	{
		$requete = "SELECT modepaiement FROM modepaiement";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

		//PAIEMENT

	function listepaiement()
	{
		$requete = "SELECT * FROM paiement";
		try
		{
			$con = dbconnect();
			$state = $con->query($requete);
			$donne = array();
			while($d=$state->fetch()){
					$donne[]=$d;
			}
		}
		catch (PDOException $e) {
        	print "Erreur ! : " . $e->getMessage();
        	die();
        }
		return $donne;
	}

	//-------------------------------------------------------------------INSERTION

		//CATEGORIE

	function ajoutercategorie($nom)
	{
		$req = "INSERT INTO categorie(nomcategorie) VALUES('".$nom."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//MARQUE

	function ajoutermarque($nom)
	{
		$req = "INSERT INTO marque(nommarque) VALUES('".$nom."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//MODEPAIEMENT

	function ajoutermodepaiement($nom)
	{
		$req = "INSERT INTO modepaiement(modepaiement) VALUES('".$nom."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//CLIENT

	function ajouterclient($nom,$identifiant,$motdepasse,$solde,$telephone,$adresse)
	{
		$req = "INSERT INTO client(nom,identifiant,motdepasse,solde,telephone,adresse) values('".$nom."','".$identifiant."','".$motdepasse."',".$solde.",".$telephone.",'".$adresse."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//ARTICLE

	function ajouterarticle($categorie,$nom,$description,$marque,$pointure,$prix,$dateajout)
	{
		$req = "INSERT INTO article(categorie,nom,description,marque,pointure,prix,dateajout) values('".$categorie."','".$nom."','".$description."','".$marque."','".$pointure."',".$prix.",'".$dateajout."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//PANIER

	function ajouterpanier($article,$prix,$quantite,$montant,$dateachat,$client)
	{
		$req = "INSERT INTO panier(article,prix,quantite,montant,dateachat,client) values('".$article."',".$prix.",".$quantite.",".$montant.",'".$dateachat."','".$client."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

	function ajouterhistoriquepanier($article,$prix,$quantite,$montant,$dateachat,$client)
	{
		$req = "INSERT INTO historiquepanier(article,prix,quantite,montant,dateachat,client) values('".$article."',".$prix.",".$quantite.",".$montant.",'".$dateachat."','".$client."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//PAIEMENT

	function ajouterpaiement($dateachat,$client,$montant,$modepaiement)
	{
		$req = "INSERT INTO paiement(article,prix,quantite,montant,dateachat,client) values('".$dateachat."','".$client."',".$montant."'".$modepaiement."')";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

	//----------------------------------------------------------------------DELETE

		//PANIER

	function deletepanier()
	{
		$req = "DELETE FROM panier";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

	function deletepanier_nom($nom)
	{
		$req = "DELETE FROM panier WHERE article = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//HISTORIQUE

	function deletehistorique($nom)
	{
		$req = "DELETE FROM historiquepanier WHERE article = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//ARTICLE

	function deletearticle($nom)
	{
		$req = "DELETE FROM panier WHERE nom = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//CATEGORIE

	function deletecategorie($nom)
	{
		$req = "DELETE FROM categorie WHERE nomcategorie = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//MARQUE

	function deletemarque($nom)
	{
		$req = "DELETE FROM marque WHERE nommarque = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//CLIENT

	function deleteclient($nom)
	{
		$req = "DELETE FROM client WHERE nom = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

		//MODPAIEMENT

	function deletemodepaiement($nom)
	{
		$req = "DELETE FROM modepaiement WHERE modepaiement = '".$nom."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
	}

	//----------------------------------------------------------------------UPDATE

		//PATIENT

	function updatesolde($identifiant,$solde)
	{
		$req = "UPDATE client SET solde = ".$solde." WHERE identifiant = '".$identifiant."'";
		try
		{
			$con = dbconnect();
			$con->query($req);
		}
		catch(PDOException $e)
		{
			print "Erreur ! : " . $e->getMessage();
        	die();
		}
		echo $req;
	}

?>
