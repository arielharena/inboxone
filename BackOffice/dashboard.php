<?php  

    require('inc/fonction.php');

    $article = listearticle();
    $categorie = listecategorie();
    $marque = listemarque();
    $client = listeclient();
    $historique = listehistorique();
    $modepaie = listemodepaiementall();
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Tableau de bord</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a class="simple-text">
                    Menu
                </a>
            </div>

            <?php include('inc/menunav.php'); ?>

    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Tableau de bord</a>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des articles</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Categorie</th>
                                            <th>Nom</th>
                                            <th>Description</th>
                                            <th>Marque</th>
                                            <th>Pointure</th>
                                            <th>Prix</th>
                                            <th>Date d'ajout</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($article as $a) { ?>
                                        <tr>
                                            <td><?php echo $a['id']; ?></td>
                                            <td><?php echo $a['categorie']; ?></td>
                                            <td><?php echo $a['nom']; ?></td>
                                            <td><?php echo $a['description']; ?></td>
                                            <td><?php echo $a['marque']; ?></td>
                                            <td><?php echo $a['pointure']; ?></td>
                                            <td><?php echo $a['prix']; ?></td>
                                            <td><?php echo $a['dateajout']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des categories</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Categorie</th>    
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($categorie as $c) { ?>
                                        <tr>
                                            <td><?php echo $c['id']; ?></td>
                                            <td><?php echo $c['nomcategorie']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des modes paiement</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Mode paiement</th>   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($modepaie as $mo) { ?>
                                        <tr>
                                            <td><?php echo $mo['id']; ?></td>
                                            <td><?php echo $mo['modepaiement']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des historiques du panier</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Article</th>
                                            <th>Prix</th>
                                            <th>Quantite</th>
                                            <th>Montant</th>
                                            <th>Date achat</th>
                                            <th>Client</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($historique as $h) { ?>
                                        <tr>
                                            <td><?php echo $h['id']; ?></td>
                                            <td><?php echo $h['article']; ?></td>
                                            <td><?php echo $h['prix']; ?></td>
                                            <td><?php echo $h['quantite']; ?></td>
                                            <td><?php echo $h['montant']; ?></td>
                                            <td><?php echo $h['dateachat']; ?></td>
                                            <td><?php echo $h['client']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des marques</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Marque</th>      
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($marque as $m) { ?>
                                        <tr>
                                            <td><?php echo $m['id']; ?></td>
                                            <td><?php echo $m['nommarque']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des clients</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nom</th>
                                            <th>Identifiant</th>
                                            <th>Mot de passe</th>
                                            <th>Solde</th>
                                            <th>Telephone</th>
                                            <th>Adresse</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($client as $cl) { ?>
                                        <tr>
                                            <td><?php echo $cl['id']; ?></td>
                                            <td><?php echo $cl['nom']; ?></td>
                                            <td><?php echo $cl['identifiant']; ?></td>
                                            <td><?php echo $cl['motdepasse']; ?></td>
                                            <td><?php echo $cl['solde']; ?></td>
                                            <td><?php echo $cl['telephone']; ?></td>
                                            <td><?php echo $cl['adresse']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">

            </div>
        </footer>

    </div>
</div>


</body>

    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
	<script src="assets/js/chartist.min.js"></script>
    <script src="assets/js/bootstrap-notify.js"></script>
	<script src="assets/js/light-bootstrap-dashboard.js"></script>
	<script src="assets/js/demo.js"></script>

    <script src="assets/lib/jquery/jquery.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.dataTables.js"></script>
    <script src="assets/js/datatables.js"></script>
    <script src="assets/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/jquery.tablesorter.min.js"></script>
    <script src="assets/js/jquery.tablesorter.js"></script>

    <script>
        $(document).ready(function() {
            $('table.display').DataTable();
        } );
    </script>

</html>
