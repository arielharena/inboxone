<?php

    require('inc/fonction.php');

    $client = listeclient();

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Client</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/animate.min.css" rel="stylesheet"/>
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a class="simple-text">
                    Menu
                </a>
            </div>

            <?php include('inc/menunav.php'); ?>

    	</div>
    </div>

    <div class="main-panel">
		<nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Client</a>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Insertion</h3>
                                <h4 class="title">Ajout d'un nouveau client</h4>
                            </div>
                            <div class="content">
                                <form action="ajoutarticle.php" method="post">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Nom</label>
                                            <input type="text" class="form-control" name="nom">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Identifiant</label>
                                            <input type="text" class="form-control" name="identifiant">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>Mot de passe</label>
                                            <input type="text" class="form-control" name="mdp">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Confirmer le mot le passe</label>
                                            <input type="text" class="form-control" name="confirme">
                                        </div>
                                        <div class="col-md-4">
                                            <label>Solde</label>
                                            <input type="text" class="form-control" name="solde">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Telephone</label>
                                            <input type="text" class="form-control" name="telephone">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Adresse</label>
                                            <input type="text" class="form-control" name="adresse">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Insertion</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h3 class="title">Liste</h3>
                                <h4 class="title">Liste des clients</h4>
                            </div>
                            <div class="content">
                                <table id="dataTable" class="table table-condensed table-hover display">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Nom</th>
                                            <th>Identifiant</th>
                                            <th>Mot de passe</th>
                                            <th>Solde</th>
                                            <th>Telephone</th>
                                            <th>Adresse</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($client as $cl) { ?>
                                        <tr>
                                            <td><?php echo $cl['id']; ?></td>
                                            <td><?php echo $cl['nom']; ?></td>
                                            <td><?php echo $cl['identifiant']; ?></td>
                                            <td><?php echo $cl['motdepasse']; ?></td>
                                            <td><?php echo $cl['solde']; ?></td>
                                            <td><?php echo $cl['telephone']; ?></td>
                                            <td><?php echo $cl['adresse']; ?></td>
                                            <td><a href="suppclient.php?nom=<?php echo $cl['nom']; ?>"><button type="submit" class="btn btn-danger">Supprimer</button></a> <a href="modifier.php"><button type="submit" class="btn btn-warning">Modifier</button></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">

            </div>
        </footer>

    </div>
</div>


</body>

    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>
    <script src="assets/js/chartist.min.js"></script>
    <script src="assets/js/bootstrap-notify.js"></script>
    <script src="assets/js/light-bootstrap-dashboard.js"></script>
    <script src="assets/js/demo.js"></script>

    <script src="assets/lib/jquery/jquery.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/jquery.dataTables.js"></script>
    <script src="assets/js/datatables.js"></script>
    <script src="assets/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/js/jquery.tablesorter.min.js"></script>
    <script src="assets/js/jquery.tablesorter.js"></script>

    <script>
        $(document).ready(function() {
            $('table.display').DataTable();
        } );
    </script>

</html>