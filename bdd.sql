create database andiashoes;

use andiashoes;

create table administrateur(
	id int not null auto_increment primary key,
	nom varchar(150),
	identifiant varchar(150),
	motdepasse varchar(150)
);

insert into administrateur(nom,identifiant,motdepasse) values('Harena','harcost15','harenariel15');
insert into administrateur(nom,identifiant,motdepasse) values('Rojo','Rojoraben','root');

create table client(
	id int not null auto_increment primary key,
	nom varchar(150),
	identifiant varchar(150),
	motdepasse varchar(150),
	solde int,
	telephone int,
	adresse varchar(150)
);

insert into client(nom,identifiant,motdepasse,solde,telephone,adresse) values('Rakoto','koto32','kotokoto',1000000,0327545686,'IVD 47 Ankadifotsy');
insert into client(nom,identifiant,motdepasse,solde,telephone,adresse) values('Randria','michael09','micmic09',750000,0347865487,'Lot 03 789D Andobo');

create table article(
	id int not null auto_increment primary key,
	categorie varchar(150),
	nom varchar(150),
	description varchar(150),
	marque varchar(150),
	pointure varchar(150),
	prix int,
	dateajout date
);

create table image(
	id int not null auto_increment primary key,
	article varchar(150),
	image varchar(150)
);

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Habille','Lacost','Elegant, Style, ideal pour les soirees','Lacost','38,39,40,41,42',250000,'2018/01/02');
insert into image(article,image) values('Lacost','Lacost1');
insert into image(article,image) values('Lacost','Lacost2');
insert into image(article,image) values('Lacost','Lacost3');
insert into image(article,image) values('Lacost','Lacost4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Habille','BullBoxer','Pour des fetes chic, Elegant','BullBoxer','38,39,40,41,42',175000,'2018/01/02');
insert into image(article,image) values('BullBoxer','BullBoxer1');
insert into image(article,image) values('BullBoxer','BullBoxer2');
insert into image(article,image) values('BullBoxer','BullBoxer3');
insert into image(article,image) values('BullBoxer','BullBoxer4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Habille','Geox','Resistant tout en etant elegant, pour des occasions special','Geox','38,39,40,41,42',225000,'2018/01/02');
insert into image(article,image) values('Geox','Geox1');
insert into image(article,image) values('Geox','Geox2');
insert into image(article,image) values('Geox','Geox3');
insert into image(article,image) values('Geox','Geox4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Habille','Pikolinos','Facile a entreprendre, Habille mais passe par tout','Pikolinos','38,39,40,41,42',150000,'2018/01/02');
insert into image(article,image) values('Pikolinos','Pikolinos1');
insert into image(article,image) values('Pikolinos','Pikolinos2');
insert into image(article,image) values('Pikolinos','Pikolinos3');
insert into image(article,image) values('Pikolinos','Pikolinos4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Habille','Yep','Botte a la mode, assortit avec tout vetement','Yep','38,39,40,41,42',150000,'2018/01/02');
insert into image(article,image) values('Yep','Yep1');
insert into image(article,image) values('Yep','Yep2');
insert into image(article,image) values('Yep','Yep3');
insert into image(article,image) values('Yep','Yep4');


insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Sandale','Grendha','Sandale a hauteur, ayant une longue duree d utilisation','Grendha','38,39,40,41,42',75000,'2018/03/01');
insert into image(article,image) values('Grendha','Grendha1');
insert into image(article,image) values('Grendha','Grendha2');
insert into image(article,image) values('Grendha','Grendha3');
insert into image(article,image) values('Grendha','Grendha4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Sandale','Havaianas','Sandale tres pratique pour fille, simple','Havaianas','38,39,40,41,42',85000,'2018/03/01');
insert into image(article,image) values('Havaianas','Havaianas1');
insert into image(article,image) values('Havaianas','Havaianas2');
insert into image(article,image) values('Havaianas','Havaianas3');
insert into image(article,image) values('Havaianas','Havaianas4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Sandale','Merrel','Fait a partir de cuire, simple, dur, resistant','Merrel','38,39,40,41,42',115000,'2018/03/01');
insert into image(article,image) values('Merrel','Merrel1');
insert into image(article,image) values('Merrel','Merrel2');
insert into image(article,image) values('Merrel','Merrel3');
insert into image(article,image) values('Merrel','Merrel4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Sandale','Quicksilver','La version pour homme, simple, pratique, ayant plusieurs motifs','Quicksilver','38,39,40,41,42',75000,'2018/03/01');
insert into image(article,image) values('Quicksilver','Quicksilver1');
insert into image(article,image) values('Quicksilver','Quicksilver2');
insert into image(article,image) values('Quicksilver','Quicksilver3');
insert into image(article,image) values('Quicksilver','Quicksilver4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Sandale','Roadsign','Une autre faite a partir de cuire, tres resistant','Roadsign','38,39,40,41,42',125000,'2018/03/01');
insert into image(article,image) values('Roadsign','Roadsign1');
insert into image(article,image) values('Roadsign','Roadsign2');
insert into image(article,image) values('Roadsign','Roadsign3');
insert into image(article,image) values('Roadsign','Roadsign4');


insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Talon','Caprice','Talon compenser, plus d equilibre, en cuire','Caprice','38,39,40,41,42',250000,'2018/05/01');
insert into image(article,image) values('Caprice','Caprice1');
insert into image(article,image) values('Caprice','Caprice2');
insert into image(article,image) values('Caprice','Caprice3');
insert into image(article,image) values('Caprice','Caprice4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Talon','Clarks','Moins compenser, plus style, pour des occasions chic','Clarks','38,39,40,41,42',325000,'2018/05/01');
insert into image(article,image) values('Clarks','Clarks1');
insert into image(article,image) values('Clarks','Clarks2');
insert into image(article,image) values('Clarks','Clarks3');
insert into image(article,image) values('Clarks','Clarks4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Talon','Dorking','Talon compenser, style cuire noire','Dorking','38,39,40,41,42',275000,'2018/05/01');
insert into image(article,image) values('Dorking','Dorking1');
insert into image(article,image) values('Dorking','Dorking2');
insert into image(article,image) values('Dorking','Dorking3');
insert into image(article,image) values('Dorking','Dorking4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Talon','Guess','Talon a aiguille, a la mode, pour des occasions a grande echelle','Guess','38,39,40,41,42',300000,'2018/05/01');
insert into image(article,image) values('Guess','Guess1');
insert into image(article,image) values('Guess','Guess2');
insert into image(article,image) values('Guess','Guess3');
insert into image(article,image) values('Guess','Guess4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Talon','Tamaris','Talon a aiguille, ideal avec tout modeles de robe','Tamaris','38,39,40,41,42',295000,'2018/05/01');
insert into image(article,image) values('Tamaris','Tamaris1');
insert into image(article,image) values('Tamaris','Tamaris2');
insert into image(article,image) values('Tamaris','Tamaris3');
insert into image(article,image) values('Tamaris','Tamaris4');


insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Tennis','Adidas','Sport, souple, a la mode, pas fait pour les activitees trop sportive','Adidas','38,39,40,41,42',250000,'2018/07/01');
insert into image(article,image) values('Adidas','Adidas1');
insert into image(article,image) values('Adidas','Adidas2');
insert into image(article,image) values('Adidas','Adidas3');
insert into image(article,image) values('Adidas','Adidas4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Tennis','Asics','Sport, dur en etant souple pour les activitees sportive','Asics','38,39,40,41,42',275000,'2018/07/01');
insert into image(article,image) values('Asics','Asics1');
insert into image(article,image) values('Asics','Asics2');
insert into image(article,image) values('Asics','Asics3');
insert into image(article,image) values('Asics','Asics4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Tennis','Converse','Standart, type de chaussure polyvalent','Converse','38,39,40,41,42',150000,'2018/07/01');
insert into image(article,image) values('Converse','Converse1');
insert into image(article,image) values('Converse','Converse2');
insert into image(article,image) values('Converse','Converse3');
insert into image(article,image) values('Converse','Converse4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Tennis','Nike','Sport, pour les passionnees de basketball, a la mode','Nike','38,39,40,41,42',450000,'2018/07/01');
insert into image(article,image) values('Nike','Nike1');
insert into image(article,image) values('Nike','Nike2');
insert into image(article,image) values('Nike','Nike3');
insert into image(article,image) values('Nike','Nike4');

insert into article(categorie,nom,description,marque,pointure,prix,dateajout) values('Tennis','Reebok','Sport, une securite mise a l epreuve pour les chevilleres','Reebok','38,39,40,41,42',550000,'2018/07/01');
insert into image(article,image) values('Reebok','Reebok1');
insert into image(article,image) values('Reebok','Reebok2');
insert into image(article,image) values('Reebok','Reebok3');
insert into image(article,image) values('Reebok','Reebok4');
	
create table categorie(
	id int not null auto_increment primary key,
	nomcategorie varchar(150)
);

insert into categorie(nomcategorie) values('Habille');
insert into categorie(nomcategorie) values('Sandale');
insert into categorie(nomcategorie) values('Talon');
insert into categorie(nomcategorie) values('Tennis');

create table marque(
	id int not null auto_increment primary key,
	nommarque varchar(150)
);

insert into marque(nommarque) values('Bullboxer');
insert into marque(nommarque) values('Lacost');
insert into marque(nommarque) values('Geox');
insert into marque(nommarque) values('Pikolinos');
insert into marque(nommarque) values('Yep');
insert into marque(nommarque) values('Grendha');
insert into marque(nommarque) values('Havaianas');
insert into marque(nommarque) values('Merrel');
insert into marque(nommarque) values('Quicksilver');
insert into marque(nommarque) values('Roadsign');
insert into marque(nommarque) values('Caprice');
insert into marque(nommarque) values('Clarks');
insert into marque(nommarque) values('Dorking');
insert into marque(nommarque) values('Guess');
insert into marque(nommarque) values('Tamaris');
insert into marque(nommarque) values('Adidas');
insert into marque(nommarque) values('Asics');
insert into marque(nommarque) values('Converse');
insert into marque(nommarque) values('Nike');
insert into marque(nommarque) values('Reebok');

create table panier(
	id int not null auto_increment primary key,
	article varchar(150),
	prix varchar(150),
	quantite int,
	montant int,
	dateachat date,
	client varchar(150)
);

insert into panier(article,prix,quantite,montant,dateachat,client) values('',,,,'','');

create table historiquepanier(
	id int not null auto_increment primary key,
	article varchar(150),
	prix varchar(150),
	quantite int,
	montant int,
	dateachat date,
	client varchar(150)
);

insert into historiquepanier(article,prix,quantite,montant,dateachat,client) values('',,,,'','');

create table paiement(
	id int not null auto_increment primary key,
	dateachat date,
	client varchar(150),
	montant int,
	modepaiement varchar(150)
);

insert into paiement(dateachat,client,montant,modepaiement) values('','',,'');

create table modepaiement(
	id int not null auto_increment primary key,
	modepaiement varchar(150)
);

insert into modepaiement(modepaiement) values('Cheque');
insert into modepaiement(modepaiement) values('Virement');
insert into modepaiement(modepaiement) values('Mobile money');
insert into modepaiement(modepaiement) values('Espece');

create table articlealaune(
	id int not null auto_increment primary key,
	article varchar(150),
);

insert into articlealaune(article) values('');