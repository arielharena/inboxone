<div class="row">
    <div class="col-sm-7 footer-copyright wow fadeIn">
        <p><strong>Andia</strong></p>
    </div>
    <div class="col-sm-5 footer-social wow fadeIn">
        <a href="https:\\www.facebook.com"><i class="fa fa-facebook"></i></a>
        <a href="https:\\www.dribbble.com"><i class="fa fa-dribbble"></i></a>
        <a href="https:\\www.twitter.com"><i class="fa fa-twitter"></i></a>
        <a href="https:\\www.pinterest.com"><i class="fa fa-pinterest"></i></a>
    </div>
</div>