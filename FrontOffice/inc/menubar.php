<ul class="nav navbar-nav navbar-right">
	<li>
		<a href="page_de_vente10.0"><i class="fa fa-home"></i></br>Accueil</a>
	</li>
	<li>
		<a href="liste_des_ventes10.0"><i class="fa fa-image"></i><br>Les produits</a>
	</li>
	<li>
		<a href="stockage_de_vente10.0"><i class="fa fa-shopping-cart"></i><br>Panier</a>
	</li>
	<li>
		<a href="trouves_des_ventes10.0"><i class="fa fa-search"></i><br>Recherche</a>
	</li>
	<li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-user"></i>
            <p>Compte</p>
        </a>
        <ul class="dropdown-menu">
            <li><a href="mes_ventes10.0">Mon compte</a></li>
            <li><a href="sortir_les_ventes10.0">Deconnexion</a></li>
        </ul>
    </li>
</ul>