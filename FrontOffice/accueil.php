<?php  

	session_start();

	require('inc\fonction.php');

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Andia | Accueil</title>

        <!-- CSS -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
        
        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">Andia</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<?php include('inc\menubar.php'); ?>
				</div>
			</div>
		</nav>

		<!-- Latest work -->
        <div class="work-container">
	        <div class="container">
	        	<div class="row">
		                <h1>Les differentes categories disponible</h1>
	            </div>
	            <div class="row">
	            	<div class="col-sm-3">
		                <div class="work wow fadeInUp">
		                    <img src="assets/img/Categorie/Habille.jpg" alt="Lorem Website" data-at2x="assets/img/Categorie/Habille.jpg">
		                    <h3>Habille</h3>
		                    <a href="article-Habille.0"><button type="submit" class="btn btn-block">Afficher</button></a>
		                </div>
	                </div>
	                <div class="col-sm-3">
		                <div class="work wow fadeInDown">
		                    <img src="assets/img/Categorie/Sandale.jpg" alt="Ipsum Logo" data-at2x="assets/img/Categorie/Sandale.jpg">
		                    <h3>Sandale</h3>
		                    <a href="article-Sandale.0"><button type="submit" class="btn btn-block">Afficher</button></a>
		                </div>
		            </div>
		            <div class="col-sm-3">
		                <div class="work wow fadeInUp">
		                    <img src="assets/img/Categorie/Talon.jpg" alt="Dolor Prints" data-at2x="assets/img/Categorie/Talon.jpg">
		                    <h3>Talon</h3>
		                    <a href="article-Talon.0"><button type="submit" class="btn btn-block">Afficher</button></a>
		                </div>
		            </div>
		            <div class="col-sm-3">
		                <div class="work wow fadeInDown">
		                    <img src="assets/img/Categorie/Tennis.jpg" alt="Sit Amet Website" data-at2x="assets/img/Categorie/Tennis.jpg">
		                    <h3>Tennis</h3>
		                    <a href="article-Tennis.0"><button type="submit" class="btn btn-block">Afficher</button></a>
		                </div>
		            </div>
	            </div>
	        </div>
        </div>

        <div class="testimonials-container">
	        <div class="container">

	        </div>
        </div>

        <!-- Slider -->
        <div class="slider-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 slider">
                        <div class="flexslider">
                            <ul class="slides">
                                <li data-thumb="assets/img/pub/1.jpg">
                                    <img src="assets/img/pub/1.jpg">
                                </li>
                                <li data-thumb="assets/img/pub/2.jpg">
                                    <img src="assets/img/pub/2.jpg">
                                </li>
                                <li data-thumb="assets/img/pub/3.jpg">
                                    <img src="assets/img/pub/3.jpg">
                                </li>
                                <li data-thumb="assets/img/pub/4.jpg">
                                    <img src="assets/img/pub/4.jpg">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="testimonials-container">
	        <div class="container">

	        	<h1>Andia</h1>

	        </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <?php include('inc\footer.php');?>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>