<?php

    session_start();

    require('inc/fonction.php');

    $now = new DateTime(date('Y-m-d'));

    $article = $_GET['article'];

    $getprix = listearticle_article($article);

    $qt = null;

    if ($_POST['quantite'] == null || $_POST['quantite'] <= 0) {
      $qt = 1;
    }
    else {
      $qt = $_POST['quantite'];
    }

    $login = $_SESSION['nom'];

    $montant = null;
    $prix = null;

    $datea = $now->format('Y-m-d');

    foreach($getprix as $gtp)
    {
        $prix= $gtp['prix'];
        $montant = $prix*$qt;

        if($qt == 0 || $qt < 1 || $qt == null)
        {
            header('location:article_erreur_produit10.0');
        }
        else
        {
            ajouterpanier($article,$prix,$qt,$montant,$datea,$login);
            ajouterhistoriquepanier($article,$prix,$qt,$montant,$datea,$login);
            header('location:stockage_de_vente10.0');
        }
    }

?>