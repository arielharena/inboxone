<?php
	
	session_start();
	
	require('inc\fonction.php');

	$chemin = "assets\img\Chaussure";

	$catHabille = listearticle_categorie("Habille");
	$catSandale = listearticle_categorie("Sandale");
	$catTalon = listearticle_categorie("Talon");
	$catTennis = listearticle_categorie("Tennis");

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="wnomth=device-wnomth, initial-scale=1">
        <title>Andia | Les produits</title>

        <!-- CSS -->

        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslnomer/flexslnomer.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">Andia</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" nom="top-navbar-1">
					<?php include('inc\menubar.php'); ?>
				</div>
			</div>
		</nav>
        
        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-camera"></i>
                        <h1>Les Produits /</h1>
                        <p>Liste de tous les produits disponible</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="portfolio-container">
	        <div class="container">
	            <div class="row">
	            	<div class="col-sm-12 portfolio-filters wow fadeInLeft">
	            		<a href="#" class="filter-all active">Tout</a> / 
	            		<a href="#" class="filter-habille-design">Habille</a> / 
	            		<a href="#" class="filter-sandale-design">Sandale</a> / 
	            		<a href="#" class="filter-talon-design">Talon</a> /
	            		<a href="#" class="filter-tennis-design">Tennis</a>
	            	</div>
	            </div>
	            <div class="row">
	            	<div class="col-sm-12 portfolio-masonry">

	            		<?php foreach ($catHabille as $k) { ?>
		                <div class="portfolio-box habille-design">
		                	<div class="portfolio-box-container">
			                	<img src="<?php echo $chemin; ?>/Habille/<?php echo $k['nom']; ?>/<?php echo $k['nom'].'1'; ?>.jpg">
			                	<div class="portfolio-box-text">
			                		<h3> <?php echo $k['nom']; ?> </h3>
		                			<h6><strong>Categorie : <small><?php echo $k['categorie']; ?></small></strong></h6> 
		                			<h6><strong>Marque : <small><?php echo $k['marque']; ?></small></strong></h6> 
		                			<a href="article_produit10-<?php echo $k['nom'] ?>.0"><button type="submit" class="btn btn-block">voir</button></a>
			                	</div>
			                </div>
		                </div>
		                <?php } ?>

		                <?php foreach ($catSandale as $s) { ?>
		                <div class="portfolio-box sandale-design">
		                	<div class="portfolio-box-container">
			                	<img src="<?php echo $chemin; ?>/Sandale/<?php echo $s['nom']; ?>/<?php echo $s['nom'].'1'; ?>.jpg">
			                	<div class="portfolio-box-text">
			                		<h3> <?php echo $s['nom']; ?> </h3>
		                			<h6><strong>Categorie : <small><?php echo $s['categorie']; ?></small></strong></h6> 
		                			<h6><strong>Marque : <small><?php echo $s['marque']; ?></small></strong></h6> 
		                			<a href="article_produit10-<?php echo $s['nom'] ?>.0"><button type="submit" class="btn btn-block">voir</button></a>
			                	</div>
			                </div>
		                </div>
		                <?php } ?>

		                <?php foreach ($catTalon as $t) { ?>
		                <div class="portfolio-box talon-design">
		                	<div class="portfolio-box-container">
			                	<img src="<?php echo $chemin; ?>/Talon/<?php echo $t['nom']; ?>/<?php echo $t['nom'].'1'; ?>.jpg">
			                	<div class="portfolio-box-text">
			                		<h3> <?php echo $t['nom']; ?> </h3>
		                			<h6><strong>Categorie : <small><?php echo $t['categorie']; ?></small></strong></h6> 
		                			<h6><strong>Marque : <small><?php echo $t['marque']; ?></small></strong></h6> 
		                			<a href="article_produit10-<?php echo $t['nom'] ?>.0"><button type="submit" class="btn btn-block">voir</button></a>
			                	</div>
			                </div>
		                </div>
		                <?php } ?>

		                <?php foreach ($catTennis as $e) { ?>
		                <div class="portfolio-box tennis-design">
		                	<div class="portfolio-box-container">
			                	<img src="<?php echo $chemin; ?>/Tennis/<?php echo $e['nom']; ?>/<?php echo $e['nom'].'1'; ?>.jpg">
			                	<div class="portfolio-box-text">
			                		<h3> <?php echo $e['nom']; ?> </h3>
		                			<h6><strong>Categorie : <small><?php echo $e['categorie']; ?></small></strong></h6> 
		                			<h6><strong>Marque : <small><?php echo $e['marque']; ?></small></strong></h6> 
		                			<a href="article_produit10-<?php echo $e['nom'] ?>.0"><button type="submit" class="btn btn-block">voir</button></a>
			                	</div>
			                </div>
		                </div>
		                <?php } ?>

	                </div>
	            </div>
	        </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <?php include('inc\footer.php');?>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslnomer/jquery.flexslnomer-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>