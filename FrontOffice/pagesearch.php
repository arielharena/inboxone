<?php  

	session_start();

	require('inc\fonction.php');
	$chemin = "assets\img\Chaussure";

	$date1 = null;
    $date2 = null;
    $nom = null;
    $cat = null;
    $marque = null;
    $pu1 = null;
    $pu2 = null;

    if($_POST['d1'] == "")
    {
        $date1 = '2000/01/01';
    }
    else
    {
        $date1 = $_POST['d1'];
    }

    if($_POST['d2'] == "")
    {
        $date2 = '3020/01/01';
    }
    else
    {
        $date2 = $_POST['d2'];
    }

    if($_POST['nom'] == "")
    {
        $nom = $_POST['nom'];
    }
    else
    {
        $nom = $_POST['nom'];
    }

    if($_POST['pu1'] == "")
    {
        $pu1 = 1;
    }
    else
    {
        $pu1 = $_POST['pu1'];
    }

    if($_POST['pu2'] == "")
    {
        $pu2 = 9999999999999;
    }
    else
    {
        $pu2 = $_POST['pu2'];
    }

    if($_POST['categorie'] == "")
    {
        $cat = $_POST['categorie'];
    }
    else
    {
        $cat = $_POST['categorie'];
    }

    if($_POST['marque'] == "")
    {
        $marque = $_POST['marque'];
    }
    else
    {
        $marque = $_POST['marque'];
    }

    $listemultisearch = listearticle_recherche_multicritere($date1,$date2,$nom,$cat,$marque,$pu1,$pu2);

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Andia | Les produits</title>

        <!-- CSS -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"></a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<?php include('inc\menubar.php'); ?>
				</div>
			</div>
		</nav>
        
        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-camera"></i>
                        <h1>Reponse /</h1>
                        <p>Recherche multicritere</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Portfolio -->
        <div class="portfolio-container">
	        <div class="container">
	            <div class="row">
	            	<div class="col-sm-12 portfolio-masonry">
	            	
	            	<?php foreach ($listemultisearch as $k) { ?>
		                <div class="portfolio-box habille-design">
		                	<div class="portfolio-box-container">
			                	<div class="portfolio-box-text">
			                		<h3> <?php echo $k['nom']; ?> </h3>
		                			<h6><strong>Categorie : <small><?php echo $k['categorie']; ?></small></strong></h6> 
		                			<h6><strong>Marque : <small><?php echo $k['marque']; ?></small></strong></h6> 
		                			<a href="article_produit10-<?php echo $k['nom'] ?>.0"><button type="submit" class="btn btn-block">voir</button></a>
			                	</div>
			                </div>
		                </div>
		            <?php } ?>

		            </div>
	            </div>
	        </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <?php include('inc\footer.php');?>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>