<?php  

    session_start();

    require('inc\fonction.php');

    $chemin = "assets\img\Chaussure";

    $id_article = $_GET['article'];

    $article = listearticle_article($id_article);
    $donnee2 = listeimage($id_article);


?>

<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Andia | Details</title>

        <!-- CSS -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
        
        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">Andia</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<?php include('inc\menubar.php'); ?>
				</div>
			</div>
		</nav>

        <!-- Slider -->
        <div class="slider-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 slider">
                        <div class="flexslider">
                            <ul class="slides">
                                <?php foreach ($donnee2 as $k) { ?>
                                    <?php foreach ($article as $a) { ?>
                                <li data-thumb="<?php echo $chemin; ?>/<?php echo $a['categorie']; ?>/<?php echo $a['nom']; ?>/<?php echo $k['image']; ?>.jpg">
                                    <img src="<?php echo $chemin; ?>/<?php echo $a['categorie']; ?>/<?php echo $a['nom']; ?>/<?php echo $k['image']; ?>.jpg">
                                </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Presentation -->
        <div class="presentation-container">
        	<div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-condensed table-hover">
                            <tbody>
                                <?php foreach ($article as $k) { ?>
                                <tr>
                                    <th>Id</th>
                                    <td><?php echo $k['id']; ?></td>
                                </tr>
                                <tr>
                                    <th>Categorie</th>
                                    <td><?php echo $k['categorie']; ?></td>
                                </tr>
                                <tr>
                                    <th>Nom</th>
                                    <td><?php echo $k['nom']; ?></td>
                                </tr>
                                <tr>
                                    <th>Desription</th>
                                    <td><?php echo $k['description']; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <form action="article_panier_produit10-<?php echo $id_article; ?>.0" method="post">
                            <input type="number" class="form-control" placeholder="Nombre d'achat du produit" name="quantite">
                        <br>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-condensed table-hover">
                            <tbody>
                                <?php foreach ($article as $k) { ?>
                                <tr>
                                    <th>Marque</th>
                                    <td><?php echo $k['marque']; ?></td>
                                </tr>
                                <tr>
                                    <th>Pointure</th>
                                    <td><?php echo $k['pointure']; ?></td>
                                </tr>
                                <tr>
                                    <th>Prix</th>
                                    <td><?php echo $k['prix']; ?></td>
                                </tr>
                                <tr>
                                    <th>Date d'ajout</th>
                                    <td><?php echo $k['dateajout']; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-block">Ajout au panier</button>
                        </form>
                    </div>
                </div>
        	</div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <?php include('inc\footer.php');?>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>