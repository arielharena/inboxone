<?php  

	session_start();

	require('inc/fonction.php');
	$now = new DateTime(date('Y-m-d'));

	$montant = null;
	$mode = $_POST['select'];
	$datea = $now->format('Y-m-d');

	$argent = listeclient_solde($_SESSION['nom']);

	if($_GET['montant'] == 0)
	{
		$montant = 0;
	}
	else
	{
		$montant = $_GET['montant'];
	}

	foreach ($argent as $ar) {

		if($montant > $ar['solde'])
		{
			header('location:article_erreur_produit10.0');
		}
		else
		{
			ajouterpaiement($datea,$_SESSION['nom'],$montant,$mode);
			updatesolde($_SESSION['nom'], $ar['solde'] - $montant);
			deletepanier();
			header('location:liste_des_ventes10.0');
		}

	}

?>