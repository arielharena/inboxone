<?php

    session_start();

    require('inc\fonction.php');


    $login = $_SESSION['nom'];
    $listear = listepanier($login);

    $sommeanier = listepanier_somme($login);
    $modepaiement = listemodepaiement();
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Andia | Panier</title>

        <!-- CSS -->
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/media-queries.css">

        <link rel="shortcut icon" href="assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php">Andia - a super cool design agency...</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<?php include('inc\menubar.php'); ?>
				</div>
			</div>
		</nav>

        <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-shopping-cart"></i>
                        <h1>Panier /</h1>
                        <p>Listes des articles dans le panier</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- About Us Text -->
        <div class="about-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-md-2">
		                <h2>Votre panier</h2>
		            </div>
	            	<table class="table table-bordered table-condensed table-hover table-striped">
	            		<tr>
	            			<th>Article</th>
	            			<th>Quantite</th>
	            			<th>Prix</th>
	            			<th>Montant</th>
	            			<th>Suppression</th>
	            		</tr>
                        <?php foreach ($listear as $key) { ?>
	            		<tr>
	            			<td><?php echo $key['article']; ?></td>
	            			<td><?php echo $key['quantite']; ?></td>
	            			<td><?php echo $key['prix']; ?></td>
	            			<td><?php echo $key['montant']; ?></td>
	            			<td><a href="suppidpanier.php?nom=<?php echo $key['article']; ?>"><button type="submit" class="btn btn-warning">Effacer</button></a></td>
	            		</tr>
                        <?php } ?>
	            	</table>
                    
	            </div>
                <div class="row">

                    <?php foreach ($sommeanier as $s) { ?>
                    <form action="Valider.php?montant=<?php echo $s['montant']; ?>" method="post">
                        <div class="col-sm-4">
                            <h2>Somme achat</h2>
                            <h4>Somme a payer : <?php echo $s['montant']; ?></h4>
                        </div>
                        <div class="col-sm-4">
                            <h2>Mode de paiement</h2>
                            <select class="form-control" name="select">
                                <?php foreach($modepaiement as $mp) { ?>
                                <option value="<?php echo $mp['modepaiement']; ?>"><?php echo $mp['modepaiement']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <h2>Validation</h2>
                            <button type="submit" class="btn btn-block"><i class="fa fa-shopping-cart"></i> Valider</button>
                            <?php } ?>
                        </div>
                    </form> 
                </div>
	        </div>
        </div>

        <div class="testimonials-container">
	        <div class="container">

	        </div>
        </div>

        <!-- Footer -->
        <footer>
            <div class="container">
                <?php include('inc\footer.php');?>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/jquery.magnific-popup.min.js"></script>
        <script src="assets/flexslider/jquery.flexslider-min.js"></script>
        <script src="assets/js/jflickrfeed.min.js"></script>
        <script src="assets/js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>